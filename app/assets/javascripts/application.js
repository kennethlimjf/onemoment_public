// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery/jquery-migrate.min
//= require jquery.validate/jquery.validate.min
//= require modernizr/modernizr-2.6.2.min
//= require underscore/underscore.min
//= require jquery/jquery-easing-1.3
//= require jquery/jquery-djax
//= require jquery/jquery-ui.min
//= require tween_max/tween_max.min
//= require waypoints/waypoints.min
//= require imagesloaded/imagesloaded.pkgd.min
//= require jquery/jquery.magnific.popup.min
//= require isotope/isotope.pkgd.min
//= require jquery/jquery.tools.min
//= require bring/bring
//= require hammer/hammer.min
//= require jquery/jquery.smoothscroll
//= require sly/sly-1.2.3.min
//= require_tree .
