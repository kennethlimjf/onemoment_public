$ ->
  Spree.addImageHandlers = ->
    thumbnails = ($ '#product-images div.thumbnails')
    ($ '#main-image').data 'selectedThumb', ($ '#main-image img').attr('src')
    thumbnails.find('a').on 'click', (event) ->
      ($ '#main-image img').attr 'src', ($ event.currentTarget).attr('href')
      ($ '#main-image a').attr 'href', ($ event.currentTarget).attr('data-original-image')
      ($ '#main-image').data 'selectedThumb', ($ event.currentTarget).attr('href')
      ($ '#main-image').data 'selectedThumbId', ($ event.currentTarget).parent().attr('id')
      thumbnails.find('div').removeClass 'selected'
      ($ event.currentTarget).parent('div').addClass 'selected'
      false

  Spree.arrangeThumbnails = ->
    ($ '#product-thumbnails .ss-last').removeClass('ss-last')
    display_thumbnails = ($ "#product-thumbnails > div[style='display: block;']")
    for thumbnail, index in display_thumbnails
      if index % 3 == 2
        ($ thumbnail).addClass 'ss-last'

  Spree.showVariantImages = (variantId) ->
    ($ 'div.vtmb').hide()
    ($ 'div.tmb-' + variantId).show()
    Spree.arrangeThumbnails()
    currentThumb = ($ '#' + ($ '#main-image').data('selectedThumbId'))
    if not currentThumb.hasClass('vtmb-' + variantId)
      thumb = ($ ($ '#product-images div.thumbnails div:visible.vtmb').eq(0))
      thumb = ($ ($ '#product-images div.thumbnails div:visible').eq(0)) unless thumb.length > 0
      newImg = thumb.find('a').attr('href')
      originalNewImg = thumb.find('a').attr('data-original-image')
      ($ '#product-images ul.thumbnails div').removeClass 'selected'
      thumb.addClass 'selected'
      ($ '#main-image a').attr 'href', originalNewImg
      ($ '#main-image img').attr 'src', newImg
      ($ '#main-image').data 'selectedThumb', newImg
      ($ '#main-image').data 'selectedThumbId', thumb.attr('id')

  Spree.updateVariantPrice = (variant) ->
    variantPrice = variant.data('price')
    ($ '.price.selling').text(variantPrice) if variantPrice
  radios = ($ '#product-variants input[type="radio"]')

  if radios.length > 0
    selectedRadio = ($ '#product-variants input[type="radio"][checked="checked"]')
    Spree.showVariantImages selectedRadio.attr('value')
    Spree.updateVariantPrice selectedRadio

  Spree.addImageHandlers()
  Spree.arrangeThumbnails()

  radios.click (event) ->
    Spree.showVariantImages @value
    Spree.updateVariantPrice ($ this)
