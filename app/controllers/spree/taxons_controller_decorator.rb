module Spree
  TaxonsController.class_eval do

    def show
      @taxon = Taxon.find_by_permalink!(params[:id])
      return unless @taxon

      @searcher = build_searcher(params.merge(:taxon => @taxon.id))
      @products = @searcher.retrieve_products

      render 'taxons/show'
    end

  end
end