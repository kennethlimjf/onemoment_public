module Spree
  HomeController.class_eval do
    
    def index
      @taxon = Taxon.find_by_name("Featured")
      @searcher = build_searcher(params.merge(:taxon => @taxon.id))
      @products = @searcher.retrieve_products

      render 'home/index'
    end

  end
end
