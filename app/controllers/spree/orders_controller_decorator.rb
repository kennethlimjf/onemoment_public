module Spree
  OrdersController.class_eval do
    def print
      @order = Order.find_by_number!(params[:order_id])
      render :print, layout: 'print'
    end
  end
end