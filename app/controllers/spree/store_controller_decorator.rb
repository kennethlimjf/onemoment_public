module Spree
  BaseController.class_eval do
    Spree::Config[:layout] = 'layouts/application'
  end
end