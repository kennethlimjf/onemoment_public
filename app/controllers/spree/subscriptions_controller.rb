module Spree
  class SubscriptionsController < Spree::BaseController

    before_filter :authorize_admin, only: [:update, :destroy]

    def create
      subscription = Subscription.new(subscription_params)
      if subscription.save
        flash[:notice] = "Thank you! You have subscribed to our newsletter!"
      else
        flash[:error] = subscription.errors.full_messages.join(". ")
      end
      redirect_to root_path
    end

    def update
      subscription = Subscription.find_by(id: params[:id])
      subscription.update_attribute(:active, !subscription.active) 
      redirect_to subscriptions_admin_reports_path
    end

    def destroy 
      subscription = Subscription.find_by(id: params[:id])
      subscription.delete
      redirect_to subscriptions_admin_reports_path
    end

    private
      def subscription_params
        params.require(:subscription).permit(:email, :active)
      end

      def action
        params[:action].to_sym
      end

      def authorize_admin
        if respond_to?(:model_class, true) && model_class
          record = model_class
        else
          record = controller_name.to_sym
        end
        authorize! :admin, record
        authorize! action, record
      end
  end
end