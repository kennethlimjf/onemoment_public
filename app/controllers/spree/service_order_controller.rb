module Spree
  class ServiceOrderController < Spree::StoreController

    def new
      @selected = params[:select]
      render 'service_order/new'
    end

    def submit
      sof = Spree::ServiceOrderForm.new(service_order_params)
      if sof.valid?
        ServiceOrderMailer.service_order_request(sof).deliver
        flash[:notice] = "Your request has been sent. We will get back to you shortly! :)"
        redirect_to service_order_path
      else
        flash[:error] = "There are some problems with your particulars. Please fill it up correctly."
        render 'service_order/new'
      end
    end


    private
      def service_order_params
        params.permit(:package, :name, :email, :phone, :ocassion_date, :comments)
      end
  end
end
