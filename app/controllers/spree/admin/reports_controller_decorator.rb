module Spree
  module Admin
    ReportsController.class_eval do

      def initialize
        super
        ReportsController.add_available_report!(:sales_total)
        ReportsController.add_available_report!(:subscriptions)
      end

      def subscriptions
        @subscriptions = Subscription.all
      end

      def subscriptions_csv
        send_data Subscription.to_csv, filename: "onemoment_subscriptions.csv", type: "text/csv"
      end
    end
  end
end
