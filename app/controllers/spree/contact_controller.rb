module Spree
  class ContactController < Spree::StoreController
    def new
      render 'contact/new'
    end

    def submit
      cr = Spree::ContactRequest.new(contact_params)
      if cr.valid?
        ContactMailer.contact_request(cr).deliver
        flash[:notice] = "Your message has been sent. We will get back to you shortly! :)"
        redirect_to contact_path
      else
        flash[:error] = "There are some problems with your particulars. Please fill it up correctly."
        render :contact
      end
    end


    private
      def contact_params
        params.permit(:email, :phone, :service, :message)  
      end
  end
end