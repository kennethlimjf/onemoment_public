module Spree
  ProductsController.class_eval do
    def show
      @variants = @product.variants_including_master.active(current_currency).includes([:option_values, :images])
      @product_properties = @product.product_properties.includes(:property)
      if params[:taxon_id]
        @taxon = Spree::Taxon.find(params[:taxon_id]) 
      elsif @product.taxons.any?
        @taxon = @product.taxons.first
      else
        @taxon = ""
      end

      render 'products/show'
    end
  end
end