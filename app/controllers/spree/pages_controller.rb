module Spree
  class PagesController < Spree::StoreController
    def about
      render 'pages/about'
    end

    def services
      render 'pages/services'
    end

    def services_theme_decor
      render 'pages/services_theme_decor'
    end

    def services_theme_nautical
      render 'pages/services_theme_nautical'
    end

    def services_theme_summer
      render 'pages/services_theme_summer'
    end

    def services_theme_romance
      render 'pages/services_theme_romance'
    end

    def services_theme_asian
      render 'pages/services_theme_asian'
    end

    def services_theme_glamor
      render 'pages/services_theme_glamor'
    end

    def services_customised_stationaries
      render 'pages/services_customised_stationaries'
    end

    def services_wedding_planning
      render 'pages/services_wedding_planning'
    end

    def privacy
      render 'pages/privacy'
    end

    def delivery
      render 'pages/delivery'
    end

    def terms
      render 'pages/terms'
    end
  end
end