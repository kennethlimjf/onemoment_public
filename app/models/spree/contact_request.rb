module Spree
  class ContactRequest
    attr_accessor :email, :phone, :service, :message

    def initialize(options={})
      self.email = options[:email]
      self.phone = options[:phone]
      self.service = options[:service]
      self.message = options[:message]
    end

    def valid?
      if ( email && phone && service && message &&
          !email.strip.empty? && !phone.strip.empty? && !service.strip.empty? && !message.strip.empty? )
        return true 
      else
        return false
      end
    end
  end
end