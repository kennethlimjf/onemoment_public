module Spree
  class ServiceOrderForm

    attr_accessor :package, :name, :email, :phone, :ocassion_date, :comments

    def initialize(options={})
      self.package = options[:package]
      self.name = options[:name]
      self.email = options[:email]
      self.phone = options[:phone]
      self.ocassion_date = options[:ocassion_date]
      self.comments = options[:comments]
    end

    def valid?
      if ( package && name && email && phone && ocassion_date &&
          !package.strip.empty? && !name.strip.empty? && !email.strip.empty? &&
          !phone.strip.empty? && !ocassion_date.strip.empty? )
        return true 
      else
        return false
      end
    end

  end
end