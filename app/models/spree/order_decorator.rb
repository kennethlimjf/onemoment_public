module Spree
  Order.class_eval do

    state_machine.before_transition :to => :payment, :do => :requested_delivery_date_is_valid?

    def requested_delivery_date_is_valid?
      if requested_delivery_date && requested_delivery_date != ""
        return true if ( DateTime.parse(requested_delivery_date) > 1.week.from_now )
      end
      errors.add(:requested_delivery_date, "earliest possible is at least 1 week from now")
      false
    end
  end
end

