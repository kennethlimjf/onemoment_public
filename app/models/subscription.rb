class Subscription < ActiveRecord::Base
  validates_presence_of :email
  validates_uniqueness_of :email
  validates_presence_of :active

  def self.to_csv
    CSV.generate do |csv|
      csv << column_names
      all.each do |subscription|
        csv << subscription.attributes.values_at(*column_names)
      end
    end
  end
  
end
