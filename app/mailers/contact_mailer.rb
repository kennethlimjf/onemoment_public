class ContactMailer < ActionMailer::Base

  def contact_request(cr)
    @email, @phone, @service, @message = cr.email, cr.phone, cr.service, cr.message
    mail(from: "#{@email}", to: 'onemomentsg@gmail.com', subject: "[Contact Request] From #{@email}")
  end
  
end