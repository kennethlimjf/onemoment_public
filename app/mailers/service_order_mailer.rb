class ServiceOrderMailer < ActionMailer::Base

  def service_order_request(service_order_form)
    @service_order_form = service_order_form
    mail(from: "#{@service_order_form.email}", to: 'onemomentsg@gmail.com', subject: "[Service Order Request] From #{@service_order_form.email}")
  end

end