Rails.application.routes.draw do
  root to: "spree/home#index"
  mount Spree::Core::Engine, :at => '/'
end

Spree::Core::Engine.routes.draw do
  get '/about', to: 'pages#about', as: :about
  get '/services', to: 'pages#services', as: :services
  get '/services/theme-reception-decoration', to: 'pages#services_theme_decor', as: :services_theme_decor
  get '/services/theme-reception-decoration/nautical-theme', to: 'pages#services_theme_nautical', as: :services_theme_nautical
  get '/services/theme-reception-decoration/summer-theme', to: 'pages#services_theme_summer', as: :services_theme_summer
  get '/services/theme-reception-decoration/romance-theme', to: 'pages#services_theme_romance', as: :services_theme_romance
  get '/services/theme-reception-decoration/glamor-theme', to: 'pages#services_theme_glamor', as: :services_theme_glamor
  get '/services/theme-reception-decoration/asian-theme', to: 'pages#services_theme_asian', as: :services_theme_asian
  get '/services/customised-stationaries', to: 'pages#services_customised_stationaries', as: :services_customised_stationaries
  get '/services/wedding-planning', to: 'pages#services_wedding_planning', as: :services_wedding_planning
  get '/privacy', to: 'pages#privacy', as: :privacy
  get '/delivery', to: 'pages#delivery', as: :delivery
  get '/terms', to: 'pages#terms', as: :terms
  get '/service-order', to: 'service_order#new', as: :service_order
  post '/service-order', to: 'service_order#submit'
  get '/contact', to: 'contact#new', as: :contact
  post '/contact', to: 'contact#submit'
  get 'orders/:order_id/print', to: 'orders#print', as: :print_order

  # Subscriptions
  resources :subscriptions, only: [:create, :update, :destroy]
  get 'reports/subscriptions', to: 'admin/reports#subscriptions', as: :subscriptions_admin_reports
  get 'reports/subscriptions/csv', to: 'admin/reports#subscriptions_csv', as: :subscriptions_admin_reports_csv
end
