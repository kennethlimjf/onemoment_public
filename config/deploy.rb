# config valid only for Capistrano 3.1
lock '3.2.1'

set :application, 'spree_base'

set :stages, ["production"]
set :default_stage, "production"
set :rails_env, "production"

set :scm, :git
set :branch, 'master'
set :repo_url, 'git_repo_url'

set :deploy_to, "/path/to/spree_base"
set :use_sudo, false

set :keep_releases, 3

set :rbenv_type, :user
set :rbenv_ruby, '2.1.2'
set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles, :all # default value



namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute "kill `cat /tmp/unicorn.spree_base.pid`"
      execute "cd /path/to/spree/spree_base/current ; ( RAILS_ENV=production RBENV_ROOT=~/.rbenv RBENV_VERSION=2.1.2 RBENV_ROOT=~/.rbenv RBENV_VERSION=2.1.2 ~/.rbenv/bin/rbenv exec bundle exec unicorn_rails -c config/unicorn.rb -D )"
    end
  end

  desc "Upload local_env.yml containing environment variables"
  task :upload_keys do
    on roles(:app) do
      execute "rm -rf /path/to/spree/spree_base/shared/config/local_env.yml"
      upload! StringIO.new(File.read("config/local_env.yml")), "/path/to/spree/spree_base/shared/config/local_env.yml"
    end
  end

  desc "Symlink local_env.yml file"
  task :symlink_keys do
    on roles(:app) do
      execute "ln -sfn #{shared_path}/config/local_env.yml #{release_path}/config/local_env.yml"
    end
  end

  desc "Upload database.yml file"
  task :upload_db do
    on roles(:app) do
      execute "rm -rf /path/to/spree/spree_base/shared/config/database.yml"
      upload! StringIO.new(File.read("config/database.yml")), "/path/to/spree/spree_base/shared/config/database.yml"
    end
  end

  desc "Symlink database.yml file"
  task :symlink_db do
    on roles(:app) do
      execute "ln -sfn #{shared_path}/config/database.yml #{release_path}/config/database.yml"
    end
  end

  desc "Symlink spree images folder with shared folder"
  task :symlink_images do
    on roles(:app) do
      execute "rm -rf #{release_path}/public/spree"
      execute "ln -nfs #{shared_path}/spree #{release_path}/public/spree"
    end
  end

  after :updating, :upload_db
  after :upload_db, :symlink_db
  after :symlink_db, :upload_keys
  after :upload_keys, :symlink_keys
  after :symlink_keys, :symlink_images

  after :publishing, :restart

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end
end



namespace :unicorn do
  desc "Zero-downtime restart of Unicorn"
  task :restart do
    on roles(:app) do
      execute "kill -s USR2 `cat /tmp/unicorn.spree.pid`"
    end
  end


  desc "Start unicorn"
  task :start do
    on roles(:app) do
      execute "cd /path/to/spree/spree_base/current ; ( RAILS_ENV=production RBENV_ROOT=~/.rbenv RBENV_VERSION=2.1.2 RBENV_ROOT=~/.rbenv RBENV_VERSION=2.1.2 ~/.rbenv/bin/rbenv exec bundle exec unicorn_rails -c config/unicorn.rb -D )"
    end
  end

  desc "Stop unicorn"
  task :stop do
    on roles(:app) do
      execute "kill `cat /tmp/unicorn.spree_base.pid`"
    end
  end
end