require 'spec_helper'

describe Spree::Order do
  
  describe '#requested_delivery_date_is_valid?' do

    it 'returns false when requested delivery date is empty' do
      order = Spree::Order.new
      order.requested_delivery_date = nil
      expect(order.requested_delivery_date_is_valid?).to be_falsey
    end

    it 'returns false when requested delivery date is less than 1 week from now' do
      order = Spree::Order.new
      order.requested_delivery_date = (Time.now + 6.days).strftime("%Y-%m-%d")
      expect(order.requested_delivery_date_is_valid?).to be_falsey
    end

    it 'sets error when requested delivery date is invalid' do
      order = Spree::Order.new
      order.requested_delivery_date = (Time.now + 7.days).strftime("%Y-%m-%d")
      order.requested_delivery_date_is_valid?
      expect(order.errors.full_messages.count).to eq 1
    end

    it 'returns true when requested delivery date is valid' do
      order = Spree::Order.new
      order.requested_delivery_date = (Time.now + 8.days).strftime("%Y-%m-%d")
      expect(order.requested_delivery_date_is_valid?).to be_truthy
    end

    it 'returns false when empty string' do
      order = Spree::Order.new
      order.requested_delivery_date = ""
      expect(order.requested_delivery_date_is_valid?).to be_falsey
    end
    
  end
end