require 'spec_helper'

describe SubscriptionsController do

  describe "POST create" do
    it 'creates new subscription record when given valid email address' do
      expect{ post :create, subscription: { email: "example@yahoo.com", active: true } }.to change{ Subscription.count }.by 1
    end
    it 'creates no subscription when email is invalid or empty' do
      expect{ post :create, subscription: { email: nil, active: true } }.to change{ Subscription.count }.by 0
    end
  end

end
