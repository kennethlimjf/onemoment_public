# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


# Spree::Core::Engine.load_seed if defined?(Spree::Core)
# Spree::Auth::Engine.load_seed if defined?(Spree::Auth)

# DB Seed for Singapore only e-commerce 
Spree::Country.create!([
  { name: "Singapore", iso3: "SGP", iso: "SG", iso_name: "SINGAPORE", numcode: "702" },
])
Spree::Config[:default_country_id] = Spree::Country.find_by(name: "Singapore").id
Spree::Role.where(:name => "admin").first_or_create
Spree::Role.where(:name => "user").first_or_create

# Possibly already created by a migration.
unless Spree::Store.where(code: 'spree').exists?
  Spree::Store.new do |s|
    s.code              = 'spree'
    s.name              = 'Spree Demo Site'
    s.url               = 'demo.spreecommerce.com'
    s.mail_from_address = 'spree@example.com'
  end.save!
end

# Create zone for Singapore
singapore_zone = Spree::Zone.create!(name: "Singapore", description: "Singapore only zone")
singapore_zone.zone_members.create!(zoneable: Spree::Country.find_by!(name: 'Singapore'))

Spree::Auth::Engine.load_seed if defined?(Spree::Auth)