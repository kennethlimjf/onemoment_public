class AddDeliveryDateToSpreeOrders < ActiveRecord::Migration
  def change
    add_column :spree_orders, :requested_delivery_date, :string
  end
end
