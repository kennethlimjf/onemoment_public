class CreateSubscription < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.string :email
      t.boolean :active
      t.timestamps
    end
  end
end
